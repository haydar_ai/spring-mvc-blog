package alexp.blog.service;

import alexp.blog.model.*;
import alexp.blog.repository.PostRatingRepository;
import alexp.blog.repository.PostRepository;
import alexp.blog.repository.TagRepository;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PostServiceTest {

    @Mock
    private PostRepository postRepository;

    @Mock
    private TagRepository tagRepository;

    @Mock
    private UserService userService;

    @Mock
    private PostRatingRepository postRatingRepository;

    @InjectMocks
    private PostServiceImpl postService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldGetPost() {
        final long postId = 1L;

        Post post = new Post();
        post.setId(postId);

        when(postRepository.findOne(postId))
                .thenReturn(post);

        Post retrievedPost = postService.getPost(postId);

        assertThat(retrievedPost, is(equalTo(post)));

        verify(postRepository, times(1)).findOne(postId);
    }

    @Test
    public void shouldReturnNullWhenPostNotExists() {
        final long postId = 1L;

        Post retrievedPost = postService.getPost(postId);

        assertThat(retrievedPost, is(equalTo(null)));

        verify(postRepository, times(1)).findOne(postId);
    }

    @Test
    public void shouldAddNewPost() {
        PostEditDto postEditDto = new PostEditDto();
        postEditDto.setText("short text " + Post.shortPartSeparator() + " full text");
        postEditDto.setTags("c++, hello world");

        Post post = postService.saveNewPost(postEditDto);

        assertThat(post.getShortTextPart(), containsString("short text"));
        assertThat(post.getShortTextPart(), not(containsString("full text")));

        assertThat(post.getFullPostText(), allOf(containsString("full text"), containsString("full text"), containsString(Post.shortPartSeparator())));

        assertThat(post.getTags().size(), is(equalTo(2)));

        assertThat(post.isHidden(), CoreMatchers.is(equalTo(false)));

        assertThat(post.getDateTime().toLocalDate().equals(LocalDate.now()), is(equalTo(true)));

        verify(tagRepository, times(2)).findByNameIgnoreCase(Matchers.anyString());

        verify(postRepository, times(1)).saveAndFlush(Matchers.any(Post.class));
    }

    @Test
    public void shouldEditPost() {
        LocalDateTime dt = LocalDateTime.of(2017, Month.OCTOBER, 1, 12, 30, 0);

        Long postId = 1L;

        Post oldPost = new Post();
        oldPost.setId(postId);
        oldPost.setDateTime(dt);
        oldPost.getTags().add(new Tag("tag"));

        PostEditDto postEditDto = new PostEditDto();
        postEditDto.setId(postId);
        postEditDto.setText("short text " + Post.shortPartSeparator() + " full text");
        postEditDto.setTags("c++, hello world");

        when(postRepository.findOne(postId)).thenReturn(oldPost);

        Post post = postService.updatePost(postEditDto);

        assertThat(post.getShortTextPart(), containsString("short text"));
        assertThat(post.getShortTextPart(), not(containsString("full text")));

        assertThat(post.getFullPostText(), allOf(containsString("full text"), containsString("full text"), containsString(Post.shortPartSeparator())));

        assertThat(post.getTags().size(), is(equalTo(2)));

        assertThat(post.getDateTime(), is(dt));

        verify(tagRepository, times(2)).findByNameIgnoreCase(Matchers.anyString());

        verify(postRepository, times(1)).saveAndFlush(Matchers.any(Post.class));
    }

    @Test
    public void shouldHidePost() {
        Long postId = 1L;

        Post post = new Post();
        post.setHidden(false);

        when(postRepository.findOne(postId)).thenReturn(post);

        postService.setPostVisibility(postId, true);

        assertThat(post.isHidden(), is(CoreMatchers.equalTo(true)));

        verify(postRepository, times(1)).saveAndFlush(post);
    }

    @Test
    public void shouldUnhidePost() {
        Long postId = 1L;

        Post post = new Post();
        post.setHidden(true);

        when(postRepository.findOne(postId)).thenReturn(post);

        postService.setPostVisibility(postId, false);

        assertThat(post.isHidden(), is(CoreMatchers.equalTo(false)));

        verify(postRepository, times(1)).saveAndFlush(post);
    }

    @Test
    public void shouldDeletePost() {
        Long postId = 1L;

        Post post = new Post();

        when(postRepository.findOne(postId)).thenReturn(post);

        postService.deletePost(postId);

        verify(postRepository, times(1)).delete(post);
    }

}